#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define N 200

int main () {
	double pi;
	int term;
	pi = 0;
	int sign = 1;
	
	
	for (term = 0; term < N; term++){
		pi += sign/(2.0*term+1.0);
		sign *= -1;
	}
	
	printf ("pi is: ");
	printf ("%f", (pi*4));
	printf("\n");
		
	
}