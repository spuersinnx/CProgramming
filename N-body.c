#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#define MASS 0 
#define X_POSITION 1
#define Y_POSITION 2
#define X_VELOCITY 3
#define Y_VELOCITY 4
#define N 6
#define G 100

int main () {
	
float T, t, r, R;
float x_diff, y_diff;
double Fx[6];
double Fy[6];
double F;
int a = 0;
int x = 0;
int i, j = 0;

//prompt for time interval steps
printf ("Enter number of time intervals, T: ");
scanf("%f", &T);

//prompt for time interval	
printf("Enter time interval, t: ");
scanf("%f",&t);

	
//array of N body data
double A[6][5] = {{25.0, 400.0, 400.0, 0.0, 0.0}, {20.0, 200.0, 400.0, 3.0, 4.0}, {30.0, 50.0, 600.0, 1.0, 0.0},{50.0, 400.0, 200.0, 1.0, 0.0},{40.0, 700.0, 700.0, -1.0, 0.0},{70.0, 200.0, 100.0, -1.0, 0.0}};


printf("\nData Array\n");
printf("Mass\t      x position\t   y position\t     x velocity\t     y velocity\t\n");

for (i = 0; i < 6; i++){
		for (j = 0; j < 5; j++) {
						printf("%8.1f\t", A[i][j]);
		}
		printf("\n");
}

	//initialize forces in x direction and y direction
	for (t = 0; t<T; t++) {
		for (a = 0; a < N; a++){
			Fx[a] = 0.0;
			Fy[a] = 0.0;
		}
	}
	
	//perform calculations
	for (x=0; x < N; x++){
		for (a=0; a < N; a++) {
			if (a != x) {
				x_diff = A[a][X_POSITION] - A[x][X_POSITION]; //position in x direction
				y_diff = A[a][Y_POSITION] - A[x][Y_POSITION]; //position in y direction
				R = (x_diff*x_diff + y_diff*y_diff); //distance R
				r = sqrt(R); //sqrt of R
				if (r< 10.0) {
					A[a][MASS] = 0;
					A[x][MASS] = 0; //make masses 0 if bodies get too cloase
				}
				F = G * A[a][MASS] * A[x][MASS] / r; //calculate force
				Fx[x] += F * x_diff/r;
				Fy[x] += F * y_diff/r;
			}
		}
	}
	//update all array values
	for (a = 0; a < N; a++) {
		if (A[a][MASS] != 0) {
			A[a][X_VELOCITY] += Fx[a] * t / A[a][MASS];
			A[a][Y_VELOCITY] += Fy[a] * t /A[a][MASS];
			
			A[a][X_POSITION] += A[a][X_VELOCITY] * t;
			A[a][Y_POSITION] += A[a][Y_VELOCITY] * t;
		}
	}
	
	

printf("\n Final Data Array \n"); // print out final array
printf("Body \tMass \tx position \t y position \tx velocity \ty velocity \n");
	for (i = 0; i < N; i++)
printf(" %3d %8.2f \t%10.2f \t%10.2f \t%10.2f \t%10.2f\n", i,
A[i][MASS],A[i][X_POSITION],A[i][Y_POSITION],A[i][X_VELOCITY],A[i][Y_VELOCITY]);
	
}